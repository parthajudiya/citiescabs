<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="irstheme">
    <title>Cities Cabs </title>
    <link href="<?= base_url(); ?>/assets/css/themify-icons.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/flaticon.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/animate.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/owl.carousel.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/owl.theme.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/slick.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/slick-theme.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/owl.transitions.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/odometer-theme-default.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/jquery.bxslider.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/assets/css/responsive.css" rel="stylesheet">
	<link rel="shortcut icon" href="<?= base_url(); ?>/assets/images/favicon.png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- start page-wrapper -->
    <div class="page-wrapper">
        <!-- start preloader -->
        <div class="preloader">
            <div class="lds-ripple">
                <div></div>
                <div></div>
            </div>
        </div>
        <!-- end preloader -->
        <!-- Start header -->
        <header id="header" class="site-header header-style-2">
            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?=base_url()?>"><img src="<?= base_url(); ?>/assets/images/logo.png" width="70%" alt></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navigation-holder navbar-right">
                        <button class="close-navbar"><i class="ti-close"></i></button>
                        <ul class="nav navbar-nav">
                            <li><a href="<?=base_url()?>" >Home</a></li>
                            <li><a href="<?=base_url("about")?>">About</a></li>
                            <li><a href="<?=base_url("services")?>">Services</a></li>
                            <li><a href="<?=base_url("blog")?>">Blog</a></li>
                            <li><a href="<?=base_url("contact")?>">Contact</a></li>
                        </ul>
                    </div><!-- end of nav-collapse -->
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->
        