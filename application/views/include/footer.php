<!-- .hx-site-footer-area start -->
<div class="hx-site-footer-area">
            <div class="hx-site-footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 footer-t">
                            <div class="hx-site-footer-text">
                                <div class="hx-site-logo">
                                    <img src="<?= base_url(); ?>/assets/images/logo.png" alt="">
                                </div>
                                <p>Rajkot Taxi offers highly professional & premium services for Special Guests.</p>
                                <div class="social">
                                    <ul class="d-flex">
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 footer-t">
                            <div class="hx-site-footer-link">
                                <h3>Recent post</h3>
                                <div class="hx-latest-section">
                                    <div class="posts">
                                        <div class="post">
                                            <div class="img-holder">
                                                <img src="<?= base_url(); ?>/assets/images/footer/img-1.jpg" alt>
                                            </div>
                                            <div class="details">
                                                <p>Most Importent Issue For your car.</p>
                                                <span>18 Feb 2019</span>
                                            </div>
                                        </div>
                                        <div class="post">
                                            <div class="img-holder">
                                                <img src="<?= base_url(); ?>/assets/images/footer/img-2.jpg" alt>
                                            </div>
                                            <div class="details">
                                                <p>Most Importent Issue For your car.</p>
                                                <span>18 Feb 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 footer-t">
                            <div class="hx-site-footer-adress">
                                <h3>Address</h3>
                                <div class="adress-section">
                                    <ul>
                                        <li>Head Office Address</li>
                                        <li>121 King Street, Melbourne West, </li>
                                        <li>Australia</li>
                                    </ul>
                                    <ul class="ad">
                                        <li><span>Phone : </span> <a>97228 53883</a><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="tel:82646-51501">82646 51501</a></li>
                                        <li><span>Email : </span><a href="mailto:info@citiescabs.com">info@citiescabs.com</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6 footer-t">
                            <div class="hx-site-footer-service">
                                <h3>Services</h3>
                                <div class="service-section">
                                    <ul>
                                       <li><a href="<?=base_url()?>">Home</a></li>
										<li><a href="<?=base_url("about")?>">About</a></li>
										<li><a href="<?=base_url("services")?>">Services</a></li>
										<li><a href="<?=base_url("blog")?>">Blog</a></li>
										<li><a href="<?=base_url("contact")?>">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hx-site-footer-bottom">
                <div class="container">
                    <div class="hx-site-footer-bottom-content">
                        <div class="row">
                            <div class="col-12">
                                <span>Privacy Policy | © 2020 <a href="">Itinfoway</a> All rights reserved</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .hx-site-footer-area end -->

    </div>
    <!-- end of page-wrapper -->



    <!-- All JavaScript files
    ================================================== -->
    <script src="<?= base_url(); ?>/assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>/assets/js/bootstrap.min.js"></script>

    <!-- Plugins for this template -->
    <script src="<?= base_url(); ?>/assets/js/jquery-plugin-collection.js"></script>

    <!-- Custom script for this template -->
    <script src="<?= base_url(); ?>/assets/js/script.js"></script>
	<script>
		$('#myModal').on('shown.bs.modal', function () {
		  $('#myInput').focus()
		})
	</script>
</body>

</html>