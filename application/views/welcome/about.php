

        <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>About us</h2>
                        <ol class="breadcrumb">
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>About us</li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title -->

       <div class="about-page-style">
           <!-- start about-section -->
            <section class="about-section">
                <div class="content-area">
                    <div class="left-content">
                        <img src="<?=base_url(); ?>/assets/images/about/about.png" alt="">
                    </div>
                    <div class="right-content">
                        <div class="about-content">
                            <div class="section-title">
                                <h2>About Our Company</h2>
                            </div>
                            <div class="details">
                                <p>Welcome to <b>Cities Cabs</b>. Rajkot Taxi offers highly professional & premium services for Special Guests. Rajkot Taxi working in almost cities around the india.
								   Rajkot Taxi believe that these 3 are the most important aspects of taxi or cab business.Rajkot Taxi committed to offer Fare fares, Safety & Reliabilities. For a great experience travel with Ahmedabad and Rajkot Cabs & Taxi..</p>
                                <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end about-section -->
				
			
            <!-- .hx-counter-area start -->
            <div class="fun-fact-section-s2 ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="fun-fact-grids">
                                <div class="grid">
                                    <div class="hx-counter-icon">
                                        <i class="fi flaticon-worker"></i>
                                    </div>
                                    <div>
                                        <h2><span class="odometer" data-count="300">00</span>+</h2>
                                    </div>
                                    <p>Expert Technicians</p>
                                </div>
                                <div class="grid">
                                    <div class="hx-counter-icon">
                                        <i class="fi flaticon-employee"></i>
                                    </div>
                                    <div>
                                        <h2><span class="odometer" data-count="1026">00</span></h2>
                                    </div>
                                    <p>Satisfied Client</p>
                                </div>
                                <div class="grid">
                                    <div class="hx-counter-icon">
                                        <i class="fi flaticon-business-and-finance"></i>
                                    </div>
                                    <div>
                                        <h2><span class="odometer" data-count="25">00</span>+</h2>
                                    </div>
                                    <p>Years Experience</p>
                                </div>
                                <div class="grid">
                                    <div class="hx-counter-icon">
                                        <i class="fi flaticon-car"></i>
                                    </div>
                                    <div>
                                        <h2><span class="odometer" data-count="3215">00</span></h2>
                                    </div>
                                    <p>Compleate Project</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .hx-counter-area end -->
        
           