
        <!-- start of hero -->
        <section class="hero hero-static-image">
            <div class="container">
                <div class="row">
                    <div class="col col-md-6 col-sm-7">
                        <div class="slide-title">
                            <h2>Welcome in <span>Cities Cabs</span></h2>
                        </div>
                        <div class="slide-subtitle">
                            <p>Cities Cabs is a well-established cab service provider company in India, specialized in rendering a comprehensive long and short term car rental solutions.</p>
                        </div>
                        <div class="btns">
                            <a href="#" class="theme-btn-s2">Learn More</a>
                        </div>
                    </div>
                    <div class="hero-image"></div>
                </div>
            </div>
        </section>
        <!-- end of hero slider -->


        <!-- start about-section -->
        <section class="about-section">
            <div class="content-area">
                <div class="left-content">
                   <img src="<?=base_url(); ?>/assets/images/about/about-pic.png" alt>
                </div>
                <div class="right-content">
                    <div class="about-content">
                        
                        <div class="details">
                        <p class="text-justify">we are working in almost all states and cities around India. our sole purpose is to please our customers by giving them a lifetime experience with our taxi services. 
						everything with us is top notch, from the cars to the service itself. no matter if you're a customer or an employee we'll be there for you and your needs.</p>
                        <p class="text-justify">Our Mission is to provide world class <b> Car Rental Services </b>  in India with prices that compete with the ones offered by 'around the corner' local rental car providers.
						Through extensive use of Technology, we keep our operating costs low to provide excellent services, and pass on the savings to our Customers.</p>    
						
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end about-section -->
        <!--service area start -->
        <div class="service-style-1 section-padding">
            <div class="container">
                <div class="col-12">
                    <div class="section-title-s2 text-center">
                        <span>What We Do</span>
                        <h2>Our Services</h2>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="service-wrap">
                            <div class="service-icon">
                                <i class="fi flaticon-turbo"></i>
                            </div>
                            <div class="service-text">
                                <h2>Engine Repair</h2>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-4  col-sm-6">
                        <div class="service-wrap">
                            <div class="service-icon-2">
                                <i class="fi flaticon-tyre"></i>
                            </div>
                            <div class="service-text">
                                <h2>Tires Replacement</h2>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-4  col-sm-6">
                        <div class="service-wrap">
                            <div class="service-icon-3">
                                <i class="fi flaticon-car-1"></i>
                            </div>
                            <div class="service-text">
                                <h2>Transmission</h2>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-4  col-sm-6">
                        <div class="service-wrap">
                            <div class="service-icon-4">
                                <i class="fi flaticon-car-repair"></i>
                            </div>
                            <div class="service-text">
                                <h2>Diagnostic</h2>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-4  col-sm-6">
                        <div class="service-wrap">
                            <div class="service-icon-5">
                                <i class="fi flaticon-battery"></i>
                            </div>
                            <div class="service-text">
                                <h2>Batteries</h2>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-4  col-sm-6">
                        <div class="service-wrap">
                            <div class="service-icon-6">
                                <i class="fi flaticon-electricity"></i>
                            </div>
                            <div class="service-text">
                                <h2>Breaks</h2>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- service area end -->
        <!-- start fun-fact-section -->
        <section class="fun-fact-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-6 col-sm-12">
                        <div class="fun-fact-content">
                            <h2>Here’s Our Achivement. Lets Check it Out</h2>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. chunks as necessary suffered alteration.</p>
                        </div>
                    </div>
                    <div class="col col-md-6 col-sm-12">
                        <div class="fun-fact-grids clearfix">
                            <div class="grid">
                                <div>
                                    <h3><span class="odometer" data-count="300+">00</span>+</h3>
                                </div>
                                <p>Expert Technicians</p>
                            </div>
                            <div class="grid">
                                <div>
                                    <h3><span class="odometer" data-count="1026">00</span></h3>
                                </div>
                                <p>Satisfied Client</p>
                            </div>
                            <div class="grid">
                                <div>
                                    <h3><span class="odometer" data-count="25+">00</span>+</h3>
                                </div>
                                <p>Years Experience</p>
                            </div>
                            <div class="grid">
                                <div>
                                    <h3><span class="odometer" data-count="3215">00</span></h3>
                                </div>
                                <p>Compleate Project</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end fun-fact-section -->
        <!-- pricing-area -->
        <div class="pricing-section">
            <div class="container">
                <div class="col-12">
                    <div class="section-title-s2 text-center">
                        <span>Best Pricing Plan</span>
                        <h2>Services Packages</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="tabs-site-button">
                            <div class="row pricing-tabs">
                                <div class="col-md-12 col-12">
                                    <ul class="nav nav-tabs">
                                        <li class="pricing-content-1"><a data-toggle="tab" href="#turbo"><i class="fi flaticon-turbo"></i></a></li>
                                        <li class="pricing-content-2"><a data-toggle="tab" href="#tyre"><i class="fi flaticon-tyre"></i></a></li>
                                        <li class="pricing-content-3"><a data-toggle="tab" href="#car-1"><i class="fi flaticon-car-1"></i></a></li>
                                        <li class="pricing-content-4"><a data-toggle="tab" href="#repair"><i class="fi flaticon-car-repair"></i></a></li>
                                        <li class="pricing-content-5"><a data-toggle="tab" href="#battery"><i class="fi flaticon-battery"></i></a></li>
                                        <li class="pricing-content-6"><a data-toggle="tab" href="#electricity"><i class="fi flaticon-electricity"></i></a></li>
                                    </ul>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="tab-content">
                                        <div id="turbo" class="tab-pane active">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-12">
                                                    <div class="pricing-wrap">
                                                        <div class="pricing-single">
                                                            <div class="pricing-img">
                                                                <div class="pricing-text">
                                                                    <span><small>$</small>25</span>
                                                                    <h4><a href="#">Engine Repair</a></h4>
                                                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered</p>
                                                                </div>
                                                                <img src="<?=base_url(); ?>/assets/images/pricing/img-1.png" alt="">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tyre" class="tab-pane">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-12">
                                                    <div class="pricing-wrap">
                                                        <div class="pricing-single">
                                                            <div class="pricing-img-2">
                                                                <div class="pricing-text-1">
                                                                    <span><small>$</small>35</span>
                                                                    <h4><a href="#">Tires Replacement</a></h4>
                                                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered</p>
                                                                </div>
                                                                <img src="<?=base_url(); ?>/assets/images/pricing/img-2.png" alt="">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                        <div id="car-1" class="tab-pane">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-12">
                                                    <div class="pricing-wrap">
                                                        <div class="pricing-single">
                                                            <div class="pricing-img-3">
                                                                <div class="pricing-text-2">
                                                                    <span><small>$</small>45</span>
                                                                    <h4><a href="#">Transmission</a></h4>
                                                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered</p>
                                                                </div>
                                                                <img src="<?=base_url(); ?>/assets/images/pricing/img-1.png" alt="">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                        <div id="repair" class="tab-pane">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-12">
                                                    <div class="pricing-wrap">
                                                        <div class="pricing-single">
                                                            <div class="pricing-img-4">
                                                                <div class="pricing-text-3">
                                                                    <span><small>$</small>65</span>
                                                                    <h4><a href="#">Diagnostic</a></h4>
                                                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered</p>
                                                                </div>
                                                                <img src="<?=base_url(); ?>/assets/images/pricing/img-1.png" alt="">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                        <div id="battery" class="tab-pane">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-12">
                                                    <div class="pricing-wrap">
                                                        <div class="pricing-single">
                                                            <div class="pricing-img-5">
                                                                <div class="pricing-text-4">
                                                                    <span><small>$</small>50</span>
                                                                    <h4><a href="#">Batteries</a></h4>
                                                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered</p>
                                                                </div>
                                                                <img src="<?=base_url(); ?>/assets/images/pricing/img-1.png" alt="">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                        <div id="electricity" class="tab-pane">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-12">
                                                    <div class="pricing-wrap">
                                                        <div class="pricing-single">
                                                            <div class="pricing-img-6">
                                                                <div class="pricing-text-5">
                                                                    <span><small>$</small>25</span>
                                                                    <h4><a href="#">Breaks repair</a></h4>
                                                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered</p>
                                                                </div>
                                                                <img src="<?=base_url(); ?>/assets/images/pricing/img-1.png" alt="">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- pricing-area end -->
        
        <!-- start blog-section -->
        <section class="blog-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="section-title-s2 text-center">
                            <span>Our blog</span>
                            <h2>Latest News</h2>
                        </div>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="blog-grids clearfix">
                            <div class="grid">
                                <div class="entry-media">
                                    <img src="<?=base_url(); ?>/assets/images/blog/img-1.jpg" alt>
                                </div>
                                <div class="details">
                                    <h3><a href="#">Importent tips for your faviorate car.</a></h3>
                                    <ul class="entry-meta">
                                        <li>
                                            <img src="<?=base_url(); ?>/assets/images/blog/author.jpg" alt>
                                            By <a href="#">Lily Anne</a>
                                        </li>
                                        <li>Octobor 12,2018</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="entry-media">
                                    <img src="<?=base_url(); ?>/assets/images/blog/img-2.jpg" alt>
                                </div>
                                <div class="details">
                                    <h3><a href="#">Importent tips for your faviorate car.</a></h3>
                                    <ul class="entry-meta">
                                        <li>
                                            <img src="<?=base_url(); ?>/assets/images/blog/author.jpg" alt>
                                            By <a href="#">Lily Anne</a>
                                        </li>
                                        <li>Octobor 12,2018</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="entry-media">
                                    <img src="<?=base_url(); ?>/assets/images/blog/img-3.jpg" alt>
                                </div>
                                <div class="details">
                                    <h3><a href="#">Importent tips for your faviorate car.</a></h3>
                                    <ul class="entry-meta">
                                        <li>
                                            <img src="<?=base_url(); ?>/assets/images/blog/author.jpg" alt>
                                            By <a href="#">Lily Anne</a>
                                        </li>
                                        <li>Octobor 12,2018</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end blog-section -->
        <!-- .contact area start -->
        <div class="hx-contact-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-7 col-sm-12 col-12">
                        <div class="hx-contact-content">
                            <h2>Book An Appointment</h2>
                            <div class="hx-contact-form">
                                <form method="post" class="contact-validation-active" id="hx-contact-form">
                                    <div class="half-col">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Your Name">
                                    </div>
                                    <div class="half-col">
                                        <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone">
                                    </div>
                                    <div class="half-col">
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="half-col">
                                    <select name="address" id="address" class="form-control">
                                        <option disabled selected>Tires Replacement</option>
                                        <option>Transmission</option>
                                        <option>Diagnostic</option>
                                        <option>Batteries</option>
                                        <option>Breaks</option>
                                    </select>
                                    </div>
                                    <div>
                                        <textarea class="form-control" name="note"  id="note" placeholder="Description..."></textarea>
                                    </div>
                                    <div class="submit-btn-wrapper">
                                        <button type="submit" class="theme-btn-s2">Send Message</button>
                                        <div id="loader">
                                            <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
                                        </div>
                                    </div>
                                    <div class="clearfix error-handling-messages">
                                        <div id="success">Thank you</div>
                                        <div id="error"> Error occurred while sending email. Please try again later. </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="hx-contact-img">
                            <img src="<?=base_url(); ?>/assets/images/contact/img-1.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .contact area start -->

       <!-- hx-client-area start -->
       <div class="hx-client-area">
            <div class="container">
                <div class="hx-client-item">
                    <div class="Gift-carousel owl-carousel">
                        <img src="<?=base_url(); ?>/assets/images/gift/3.png" alt="clinet">
                        <img src="<?=base_url(); ?>/assets/images/gift/2.png" alt="clinet">
                        <img src="<?=base_url(); ?>/assets/images/gift/3.png" alt="clinet">
                        <img src="<?=base_url(); ?>/assets/images/gift/4.png" alt="clinet">
                        <img src="<?=base_url(); ?>/assets/images/gift/5.png" alt="clinet">
                    </div>
                </div>
            </div>
       </div>
       <!-- hx-client-area end -->
