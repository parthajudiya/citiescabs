
        <!-- end of header -->
        <div class="service-page-style">
        <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>Team</h2>
                        <ol class="breadcrumb">
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>Team</li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title --> 
        <!-- .hx-team-area start -->
        <div class="hx-team-area section-padding">
            <div class="container">
                <div class="col-12">
                    <div class="section-title-s2 text-center">
                        <h2>Our Services</h2>
                    </div>
                </div>
                <div class="row">
                    <!-- first box start --> 
                    <div class="col-md-4 col-sm-6 col-12 col-g">
                        <div class="hx-team-single">
                            <div class="hx-team-img">
                                <img src="<?=base_url(); ?>/assets/images/team/img-2.jpg" alt="">
                                <div class="SocialIcons"  >
                                        <a href="#"  data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i></a>
                                </div>
                                <div class="profileInfo">
                                    <h3>Calvy Jenefar</h3>
                                </div>
                            </div>
                        </div>
                    </div>
						 <!-- modal start --> 
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <div class="modal-dialog" role="document">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Modal title</h4>
							  </div>
							  <div class="modal-body">
								...
							  </div>
							  <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary">Save changes</button>
							  </div>
							</div>
						  </div>
						</div>
							 <!-- modal end --> 
			 <!-- first box end -->
			 <!-- second box start -->
                    <div class="col-md-4 col-sm-6 col-12 col-g">
                        <div class="hx-team-single">
                            <div class="hx-team-img">
                                <img src="<?=base_url(); ?>/assets/images/team/img-3.jpg" alt="">
                                <div class="SocialIcons">
                                         <a href="#"><i class="fa fa-info"></i></a>
                                </div>
                                <div class="profileInfo">
                                    <h3>Alexsandar Nick</h3>
                                </div>
                            </div>
                        </div>
                    </div>
					 <!-- second box end -->
					 <!-- third box start -->
                    <div class="col-md-4 col-sm-6 col-12 col-g">
                        <div class="hx-team-single">
                            <div class="hx-team-img">
                                <img src="<?=base_url(); ?>/assets/images/team/img-4.jpg" alt="">
                                <div class="SocialIcons">
                                         <a href="#"><i class="fa fa-info"></i></a>
                                </div>
                                <div class="profileInfo">
                                    <h3>David Simon</h3>
                                </div>
                            </div>
                        </div>
                    </div>
					<!-- third box end -->
					<!-- four box start -->
					<div class="col-md-4 col-sm-6 col-12 col-g">
                        <div class="hx-team-single">
                            <div class="hx-team-img">
                                <img src="<?=base_url(); ?>/assets/images/team/img-4.jpg" alt="">
                                <div class="SocialIcons">
                                         <a href="#"><i class="fa fa-info"></i></a>
                                </div>
                                <div class="profileInfo">
                                    <h3>David Simon</h3>
                                </div>
                            </div>
                        </div>
                    </div>
					<!-- four box end -->
					<!-- five box start -->
					<div class="col-md-4 col-sm-6 col-12 col-g">
                        <div class="hx-team-single">
                            <div class="hx-team-img">
                                <img src="<?=base_url(); ?>/assets/images/team/img-3.jpg" alt="">
                                <div class="SocialIcons">
                                       <a href="#"><i class="fa fa-info"></i></a>
                                </div>
                                <div class="profileInfo">
                                    <h3>David Simon</h3>
                                </div>
                            </div>
                        </div>
                    </div>
					<!-- five box end -->
					<!-- six box end -->
					<div class="col-md-4 col-sm-6 col-12 col-g">
                        <div class="hx-team-single">
                            <div class="hx-team-img">
                                <img src="<?=base_url(); ?>/assets/images/team/img-2.jpg" alt="">
                                <div class="SocialIcons">
                                        <a href="#"  data-toggle="modal" data-target="#myModal1"><i class="fa fa-info"></i></a>
                                </div>
                                <div class="profileInfo">
                                    <h3>David Simon</h3>
                                </div>
                            </div>
                        </div>
                    </div>
					<!-- modal start -->
						<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <div class="modal-dialog" role="document">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Modal title</h4>
							  </div>
							  <div class="modal-body">
								...
							  </div>
							  <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary">Save changes</button>
							  </div>
							</div>
						  </div>
						</div>
					<!-- modal end -->
					<!-- six box end -->
                </div>
            </div>
        </div>
        <!-- .hx-team-area end -->
      