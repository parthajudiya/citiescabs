<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function display($view, $data = array())
    {
        $directory = $this->router->directory;
        $class = $this->router->class;
        $method = $this->router->method;
        
        $this->load->view("include/header");

        $this->load->view(strtolower($directory . "$class/" . $view), $data);

        $this->load->view("include/footer");
    }
}
