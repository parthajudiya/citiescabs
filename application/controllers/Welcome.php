<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'core/Controller.php';
class Welcome extends Controller
{

	public function index($city="rajkot")
	{
		$this->display('index');
	}
	public function services($city="rajkot")
	{
		$this->display('services');
	}
	public function about($city="rajkot")
	{
		$this->display('about');
	}
	public function blog($city="rajkot")
	{
		$this->display('blog');
	}
	public function contact($city="rajkot")
	{
		$this->display('contact');
	}
}
